/* eslint-disable prettier/prettier */
import React, {createContext, useState} from 'react';

// create context
export const UserContext = createContext();
export const UserProvider = props => {
  const [user, setUser] = useState({
    email: 'admin@admin.com',
    password: 'admin',
    isLogin: false,
  });
  return (
    <UserContext.Provider value={{user, setUser}}>
      {props.children}
    </UserContext.Provider>
  );
};
