/* eslint-disable prettier/prettier */

import React, {useContext, useEffect, useState} from 'react';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  useColorScheme,
  View,
} from 'react-native';
import {UserContext} from '../context/User';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Login = ({navigation}) => {
  const {user, setUser} = useContext(UserContext);
  const isDarkMode = useColorScheme() === 'dark';

  useEffect(() => {
    const getData = async () => {
      try {
        const value = await AsyncStorage.getItem('userLogin');
        if (value !== null) {
          let login = JSON.parse(value);
          setUser(login);
          if (login?.isLogin) {
            navigation.navigate('Home');
          } else {
            navigation.navigate('Login');
          }
          return;
        }
      } catch (e) {
        console.log(e);
        navigation.navigate('Login');
      }
    };
    getData();
  }, [navigation, setUser]);

  const [error, setError] = useState('');
  const [values, setValues] = useState({
    email: '',
    password: '',
  });

  const handleChange = (name, value) => {
    setValues({
      ...values,
      [name]: value,
    });
  };

  const handleSubmit = async () => {
    if (!values.email || !values.password) {
      setError('Please fill all fields');
      return;
    }

    if (user.email === values.email && user.password === values.password) {
      try {
        await AsyncStorage.setItem(
          'userLogin',
          JSON.stringify({...values, isLogin: true}),
        );
        await setUser({...values, isLogin: true});
        setValues({
          email: '',
          password: '',
        });
        navigation.navigate('Home');
      } catch (e) {
        setError('Something went wrong');
      }
    } else {
      setError('Invalid email or password');
    }
  };
  return (
    <SafeAreaView style={styles.safeView}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <View>
        <Text style={styles.pageHeader}>Zeliot Task App</Text>
      </View>
      <View style={styles.formInputs}>
        <TextInput
          style={styles.formBox}
          placeholder={'Email Address'}
          name={'email'}
          defaultValue={values.email}
          onChangeText={text => handleChange('email', text)}
        />

        <TextInput
          style={styles.formBox}
          placeholder={'Password'}
          name={'password'}
          defaultValue={values.password}
          secureTextEntry={true}
          onChangeText={text => handleChange('password', text)}
        />

        <TouchableOpacity style={styles.loginButton} onPress={handleSubmit}>
          <Text style={styles.loginButtonText}>Login Now</Text>
        </TouchableOpacity>
        {error && <Text style={styles.errorText}>{error}</Text>}
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeView: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 10,
  },
  pageHeader: {
    fontSize: 25,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 30,
    color: '#000',
  },
  formInputs: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  formBox: {
    backgroundColor: '#fff',
    padding: 15,
    width: '95%',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#e0e0e0',
    marginBottom: 5,
    marginTop: 5,
    color: '#000',
  },
  loginButton: {
    backgroundColor: '#a83256',
    padding: 15,
    width: '95%',
    borderRadius: 5,
  },
  loginButtonText: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#fff',
  },
  errorText: {
    color: '#a83256',
    fontSize: 15,
    marginTop: 5,
  },
});

export default Login;
