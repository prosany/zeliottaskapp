/* eslint-disable prettier/prettier */

import React, {useContext, useEffect} from 'react';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  TouchableOpacity,
} from 'react-native';
import {UserContext} from '../context/User';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Home = ({navigation}) => {
  const isDarkMode = useColorScheme() === 'dark';
  const {user, setUser} = useContext(UserContext);

  const onLogout = async () => {
    try {
      await AsyncStorage.removeItem('userLogin');
      setUser({...user, isLogin: false});
      navigation.navigate('Login');
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    if (!user?.isLogin) {
      navigation.navigate('Login');
    }
  }, [navigation, user]);
  return (
    <SafeAreaView style={styles.safeView}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <View>
        <Text style={styles.pageHeader}>Congrulations You're LoggedIn</Text>
      </View>
      <TouchableOpacity style={styles.logOutButton} onPress={onLogout}>
        <Text style={styles.logOutButtonText}>Logout</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeView: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 10,
  },
  pageHeader: {
    fontSize: 25,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 30,
    color: '#000',
  },
  logOutButton: {
    backgroundColor: '#a83256',
    padding: 15,
    width: '95%',
    borderRadius: 5,
    marginTop: 20,
  },
  logOutButtonText: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#fff',
  },
});

export default Home;
