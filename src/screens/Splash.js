/* eslint-disable prettier/prettier */

import React from 'react';
import {ImageBackground} from 'react-native';

const Splash = ({navigation}) => {
  setTimeout(() => {
    navigation.navigate('Login');
  }, 3000);
  return (
    <ImageBackground
      // eslint-disable-next-line react-native/no-inline-styles
      style={{flex: 1}}
      source={require('../assets/splash.png')}>
      {/* <Text>Splash</Text> */}
    </ImageBackground>
  );
};

export default Splash;
